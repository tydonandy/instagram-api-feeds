<?php

namespace Drupal\instagram_api_feeds\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure Burst Instagram API feeds settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'instagram_api_feeds_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['instagram_api_feeds.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config        = $this->config('instagram_api_feeds.settings');
    $form['#tree'] = TRUE;

    // Initial number of accounts.
    $num_instagram_accounts = 1;
    if ($form_state->get('num_instagram_accounts') !== NULL) {
      $num_instagram_accounts = $form_state->get('num_instagram_accounts');
    }
    elseif ($config->get('num_instagram_accounts') !== NULL) {
      $num_instagram_accounts = $config->get('num_instagram_accounts');
    }

    // Container for our repeating fields.
    $form['instagram_accounts'] = [
      '#type' => 'container',
      'label' => [
        '#markup' => $this->t(
          '<h3>Instagram accounts</h3>
                <p>Add multiple Instagram accounts to fetch data from.</p>
                <p><b>URL: @url?account=YOUR_ACCOUNT</b></p>', [
                  '@url' => Url::fromRoute('instagram_api_feeds.controller')->toString(),
                ]
        ),
      ],
    ];

    // Add our Instagram account fields.
    for ($i = 0; $i < $num_instagram_accounts; $i++) {
      $form['instagram_accounts'][$i]['instagram_account_name'] = [
        '#type'          => 'textfield',
        '#title'         => $this->t('Instagram account name @num', ['@num' => ($i + 1)]),
        '#default_value' => $config->get("instagram_accounts.$i.instagram_account_name") ?? '',
      ];
    }

    // Button to add more accounts.
    $form['add_instagram_account_name'] = [
      '#type'       => 'submit',
      '#attributes' => [
        'style' => ['margin-left: 0;', 'margin-right: 0; margin-bottom: 20px;'],
      ],
      '#value'      => $this->t('Add another Instagram account'),
    ];

    // Adding tabs for each Instagram account.
    $form['options'] = [
      'label' => [
        '#markup' => $this->t(
          '<h3>Instagram account options</h3><p>Set options for each account.</p>'
        ),
      ],
    ];

    for ($i = 0; $i < $config->get('num_instagram_accounts'); $i++) {
      $account_name = $config->get("instagram_accounts.$i.instagram_account_name");

      $form['options'][$account_name] = [
        '#type'  => 'details',
        '#group' => 'base',
        '#title' => $account_name,
        '#open'  => FALSE,
      ];

      $form['options'][$account_name]['username'] = [
        '#type'          => 'textfield',
        '#default_value' => $config->get("options.$account_name.username") ?? '',
        '#title'         => $this->t('Instagram username'),
        '#description'   => $this->t('This is optional but sometimes Instagram requires login to view more posts.'),
      ];

      $form['options'][$account_name]['password'] = [
        '#type'          => 'password',
        '#default_value' => $config->get("options.$account_name.password") ?? '',
        '#title'         => $this->t('Instagram password'),
        '#description'   => $this->t('This is optional but sometimes Instagram requires login to view more posts.'),
        '#attributes'    => [
          'autocomplete' => 'off',
          'value'        => $config->get("options.$account_name.password") ?? '',
        ],
      ];

      $form['options'][$account_name]['login'] = [
        '#type'          => 'checkbox',
        '#default_value' => $config->get("options.$account_name.login") ?? '0',
        '#title'         => $this->t('Use login method'),
        '#description'   => $this->t('Either to use login session for Instagram requests.'),
      ];

      $form['options'][$account_name]['instagram_cache_lifetime'] = [
        '#type'          => 'number',
        '#default_value' => $config->get("options.$account_name.instagram_cache_lifetime") ?? 24,
        '#title'         => $this->t('Instagram posts cache lifetime'),
        '#description'   => $this->t(
          'Cache lifetime in hours (1 - 48 hours time span). Protection from spamming Instagram.'
        ),
        '#min'           => 1,
        '#max'           => 48,
        '#step'          => 1,
      ];

      $form['options'][$account_name]['posts_number'] = [
        '#type'          => 'number',
        '#default_value' => $config->get("options.$account_name.posts_number") ?? 6,
        '#title'         => $this->t('Number of posts'),
        '#description'   => $this->t(
          'How many posts to fetch.'
        ),
        '#min'           => 1,
        '#max'           => 20,
        '#step'          => 1,
      ];

      $form['options'][$account_name]['excluded_hashtags'] = [
        '#type'          => 'textfield',
        '#default_value' => $config->get("options.$account_name.excluded_hashtags") ?? '',
        '#title'         => $this->t('Exclude Instagram posts with this hashtags'),
        '#description'   => $this->t('Comma separated - with #. Example: #snow, #sunny'),
        '#size'          => 255,
        '#maxlength'     => 255,
      ];
    }

    // Submit button.
    $form['submit'] = [
      '#type'       => 'submit',
      '#attributes' => [
        'class' => ['button--primary'],
        'style' => ['margin-left: 0;', 'margin-right: 0;'],
      ],
      '#value'      => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $accounts = $form_state->getValue('instagram_accounts');

    for ($i = 0; $i < count($accounts); $i++) {
      if (empty(preg_match('/^[a-zA-Z0-9_-]*$/', $accounts[$i]['instagram_account_name']))) {
        $form_state->setErrorByName("instagram_accounts][$i][instagram_account_name",
          $this->t('Please enter a valid Instagram account name.'));
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Decide what action to take based on which button the user clicked.
    switch ($values['op']) {
      case 'Add another Instagram account':
        $this->addNewFields($form, $form_state);
        break;

      default:
        $this->finalSubmit($form, $form_state);
    }
  }

  /**
   * Handle adding new Instagram account.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  private function addNewFields(array &$form, FormStateInterface $form_state) {
    $config = $this->config('instagram_api_feeds.settings');

    // Add 1 to the number of accounts.
    $num_instagram_accounts = $config->get('num_instagram_accounts') ?? $form_state->get('num_instagram_accounts');
    $form_state->set('num_instagram_accounts', ($num_instagram_accounts + 1));

    \Drupal::messenger()->addWarning($this->t('Please submit form to finish adding new account.'));
    $form_state->setRebuild();
  }

  /**
   * Handle submit.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  private function finalSubmit(array &$form, FormStateInterface $form_state) {
    $config   = $this->config('instagram_api_feeds.settings');
    $accounts = $form_state->getValue('instagram_accounts');
    $options  = $form_state->getValue('options');

    // Clear old options. Helps if account was renamed.
    $config->clear('options');

    // Store account number.
    $config->set('num_instagram_accounts', count($accounts));

    // Account base settings.
    for ($i = 0; $i < count($accounts); $i++) {
      $account = trim($accounts[$i]['instagram_account_name']);
      $config->set("instagram_accounts.$i.instagram_account_name", $account);

      // Options.
      if (isset($options[$account])) {
        $config->set("options.$account.username", $options[$account]['username']);
        $config->set("options.$account.password", $options[$account]['password']);
        $config->set("options.$account.login", $options[$account]['login']);
        $config->set("options.$account.instagram_cache_lifetime", $options[$account]['instagram_cache_lifetime']);
        $config->set("options.$account.posts_number", $options[$account]['posts_number']);
        $config->set("options.$account.excluded_hashtags", $options[$account]['excluded_hashtags']);
      }
    }

    $config->save();
    \Drupal::messenger()->addStatus($this->t('Settings saved.'));
  }

}
